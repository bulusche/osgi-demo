package com.domosafety.demo6;

import org.osgi.service.component.annotations.Component;

import com.domosafety.demo3.api.HelloService;

@Component(
		service = HelloService.class,
		property = "language=es"
		)
public class HolaService implements HelloService {

	@Override
	public String sayHelloTo(String name) {
		return "   Hola " + name;
	}

}
