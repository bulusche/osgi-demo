package com.domosafety.demo6;

import java.util.concurrent.CopyOnWriteArrayList;

import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.osgi.service.component.annotations.ReferenceCardinality;

import com.domosafety.demo3.api.HelloService;

@Component
public class ServiceUser {
	private CopyOnWriteArrayList<HelloService> services = new CopyOnWriteArrayList<>();

	private void print(){
		for (HelloService helloService : services) {
			System.out.println(helloService.sayHelloTo("Dilbert"));
		}
	}
	
	@Activate
	protected void start(){
		System.out.println("Activating");
		print();
	}
	
	@Reference(
			service = HelloService.class,
			unbind = "unbindHelloService",
			cardinality = ReferenceCardinality.MULTIPLE
			)
	protected void bindHelloService(HelloService s) {
		System.out.println("Binding...");
		services.add(s);
		print();
	}

	protected void unbindHelloService(HelloService s) {
		System.out.println("Unbinding...");
		services.remove(s);
		print();
	}


}
