package com.domosafety.demo2;

import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;

import com.google.common.primitives.Ints;

public class Demo2Activator implements BundleActivator {

	@Override
	public void start(BundleContext context) throws Exception {
		System.out.println("Demo2 starting");
		Integer tryParse = Ints.tryParse("FF", 16);
		System.out.println("Demo 2 parsed " + tryParse);

	}

	@Override
	public void stop(BundleContext context) throws Exception {
		System.out.println("Demo2 stopping");
	}

}
