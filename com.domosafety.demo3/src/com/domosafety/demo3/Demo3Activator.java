package com.domosafety.demo3;

import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceRegistration;

import com.domosafety.demo3.api.HelloService;

public class Demo3Activator implements BundleActivator {

	private ServiceRegistration<HelloService> registerService;

	@Override
	public void start(BundleContext context) throws Exception {
		System.out.println("Demo3 starting");
		
		HelloServiceImpl helloService = new HelloServiceImpl();
		registerService = context.registerService(HelloService.class, helloService, null);
	
	}

	@Override
	public void stop(BundleContext context) throws Exception {
		System.out.println("Demo3 stopping");
		registerService.unregister();
	}

}
