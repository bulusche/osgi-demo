package com.domosafety.demo3.api;

public interface HelloService {
	/**
	 * Will say hello
	 */
	String sayHelloTo(String name);

}