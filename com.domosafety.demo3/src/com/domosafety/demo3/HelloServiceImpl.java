package com.domosafety.demo3;

import com.domosafety.demo3.api.HelloService;

public class HelloServiceImpl implements HelloService {

	@Override
	public String sayHelloTo(String name){
		return "   Hello " + name;
	}
}
