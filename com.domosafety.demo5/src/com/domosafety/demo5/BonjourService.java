package com.domosafety.demo5;

import com.domosafety.demo3.api.HelloService;

public class BonjourService implements HelloService {

	@Override
	public String sayHelloTo(String name) {
		return "   Bonjour " + name;
	}

}
