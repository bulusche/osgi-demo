package com.domosafety.demo5;

import java.util.Dictionary;
import java.util.Hashtable;

import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceRegistration;

import com.domosafety.demo3.api.HelloService;

public class Demo5Activator implements BundleActivator {

	private ServiceRegistration<HelloService> registerService;

	@Override
	public void start(BundleContext context) throws Exception {
		BonjourService bonjourService = new BonjourService();
		Dictionary<String, String> properties = new Hashtable<>();
		properties.put("language", "fr");
		registerService = context.registerService(HelloService.class, bonjourService, properties );
	}

	@Override
	public void stop(BundleContext context) throws Exception {
		registerService.unregister();
		
	}

}
