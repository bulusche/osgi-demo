package com.domosafety.demo4;

import java.util.Collection;

import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceEvent;
import org.osgi.framework.ServiceListener;
import org.osgi.framework.ServiceReference;

import com.domosafety.demo3.api.HelloService;

public class Demo4Activator implements BundleActivator {

	@Override
	public void start(BundleContext context) throws Exception {
		System.out.println("Demo4 starting");
		
		Collection<ServiceReference<HelloService>> serviceReferences = context.getServiceReferences(HelloService.class, null);
		
		System.out.println("Found " + serviceReferences.size() + " services");
		
		for (ServiceReference<HelloService> next : serviceReferences) {
			HelloService service = context.getService(next);
			String sayHelloTo = service.sayHelloTo("Jack");
			System.out.println(sayHelloTo);
			context.ungetService(next);
		}

		
		context.addServiceListener(new ServiceListener() {
			@Override
			public void serviceChanged(ServiceEvent event) {
				System.out.println("Service changed " + event);
			}
		});
	}

	@Override
	public void stop(BundleContext context) throws Exception {
		System.out.println("Demo4 stoping");
		
	}

}
