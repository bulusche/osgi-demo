package com.domosafety.demo1;

import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;

import com.google.common.base.Joiner;
import com.google.common.primitives.Ints;

public class Demo1Activator implements BundleActivator {

	@Override
	public void start(BundleContext context) throws Exception {
		 
		System.out.println("Demo1 starting");
		
		Joiner joiner = Joiner.on("; ").skipNulls();
		String joined = joiner.join("Harry", null, "Ron", "Hermione...");
		
		System.out.println(joined);
//
		Integer tryParse = Ints.tryParse("24");
		System.out.println("The number is " + tryParse);
	}

	@Override
	public void stop(BundleContext context) throws Exception {
		System.out.println("Demo1 stopping");
	}

}
